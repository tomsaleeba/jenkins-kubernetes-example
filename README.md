Instructions for deploying Jenkins to kubernetes without a docker-shim and in a
way that lets Jenkins run jobs as pods in docker.

Adapted from https://www.jenkins.io/doc/book/installing/kubernetes/

Written against Jenkins v2.319.2

# Get access to a kubernetes cluster
## Option 1: minikube
1. install `minikube`, instructions at
   https://minikube.sigs.k8s.io/docs/start/
1. configure an alias so `kubectl` commands written for a "normal" k8s cluster
   will still work:
   ```bash
   alias kubectl="minikube kubectl --"
   ```
1. start the minikube cluster using containerd instead of docker and allocating
   more CPU and memory than the default. If you don't have enough resources, you
   can reduce the CPU and memory but you must also edit the StatefulSet in
   `jenkins-k8s.yml` to require less.
    ```bash
    minikube start --container-runtime=containerd --cpus=5 --memory=10g
    ```
## Option 2: a remote k8s cluster
1. I'll assume you know how to create/find a cluster and config `kubectl`

# Deploy Jenkins
1. confirm the right runtime is running. We want `containerd` to [prove that we
   are not using the
   docker-shim](https://kubernetes.io/docs/tasks/administer-cluster/migrating-from-dockershim/find-out-runtime-you-use/)
    ```bash
    $ kubectl get nodes -o wide
    ... CONTAINER-RUNTIME
    ... containerd://1.4.9
    ```
1. create namespace
    ```bash
    kubectl create ns jenkins
    ```
1. (optional) see the "Building your own Docker image locally" section further
   down if you want to build your own Docker image locally. You don't have to,
   the default one is already published to a registry.
1. create all the objects
    ```bash
    kubectl create -f jenkins-k8s.yml -n jenkins
    ```
1. watch the pods start up. If you see `ErrImagePull` or `ImagePullBackOff` then
   something is wrong. Did you build the image above, was it built *inside*
   minikube so it can access it. `minikube logs` might help.
    ```bash
    kubectl get po -n jenkins -w
    ```
1. find the URL to access the service.
    - for minikube:
        ```bash
        $ minikube service jenkins --url -n jenkins
        http://192.168.49.2:31546
        ```
    - for a remote cluster, expose the service to the public internet.
      - FIXME write these instructions
      - Jenkins seems to know when it's not on a local-ish IP (guessing 192.168...)
        and it needs HTTPS. If you try over HTTP (non-S) then you'll get errors
        about missing crumb (it's a CSRF token) when submitting forms
      - because we have HTTPS, you need an SSL cert and you need to use domain to
        access the service
1. open web UI at the URL you got from the previous step
1. get the root password with (note: you need `-p` if the pod has been recreated)
    ```bash
    kubectl logs -n jenkins -l app=jenkins-app --tail=-1 | grep -B 7 initialAdminPassword
    ```
1. create root user, or skip it. Up to you
1. set Built-In Node executors to 0:
    - Manage Jenkins -> Configure System
    - change "# of executors" to `0`
    - Save
1. configure Jenkins to use K8s
    - Manage Jenkins -> Manage Nodes and Clouds -> Configure Clouds
    - Add a new cloud -> kubernetes
    - Click "Kubernetes Cloud details..." to expand the section
    - "Kubernetes Namespace" = `jenkins`
    - get the Jenkins pod IP (`10.244.0.6` in this example)
      ```
      $ kubectl get po -n jenkins -l app=jenkins-app -o wide
      IP
      10.244.0.6
      ```
    - "Jenkins URL" = `http://<jenkins pod IP>:8080/` e.g. `http://10.244.0.6:8080/`
    - Click "Pod Template..." to expand the section
    - Click "Add Pod Template"
    - "Name" = `jenkins-agent`
    - Click "Pod Teplate Details..." to expand the section
    - "Namespace" = `jenkins`
    - "Labels" = `jenkins-agent`
    - Save
1. create a new Pipeline job, using this pipeline Groovy (bonus info: this is
   the [declarative pipeline syntax](https://www.jenkins.io/doc/book/pipeline/#declarative-pipeline-fundamentals), see further down for Scripted syntax):
    ```
    pipeline {
      agent {
        kubernetes {
          yaml '''
            spec:
              containers:
              - name: nodey
                image: node:14.17
                command:
                - sleep
                args: 
                - 99d
          '''
          defaultContainer 'nodey'
        }
      }
      stages {
        stage('Main') {
          steps {
            sh 'node --version'
          }
        }
      }
    }
    ```
1. save the job
1. run the job. If you follow the logs, you should see the `node:14.17` image
   being pulled then our command will run in the container
   ```
   + node --version
   v14.17.6
   ```

## Building your own Jenkins Docker image locally
1. build the custom docker image (we need the `docker.io` prefix, the pull in k8s
   implies it and won't find the image without it) inside minikube:
    ```bash
    minikube image build --tag docker.io/local/myjenkins:lts-slim .
    ```
1. edit `jenkins-k8s.yml` and change the `image` for the `Deployment`
    ```diff
     spec:
       containers:
       - name: jenkins
    -    image: registry.gitlab.com/tomsaleeba/jenkins-kubernetes-example
    +    image: docker.io/local/myjenkins:lts-slim
    ```

## How to get a local docker image into minikube to use for a job
This is *not* a Jenkin image, but an image for a container used inside a
pipeline.

If your image is publicly accessible, then you don't have to do this. Defining
the image in the pipeline will mean it gets pulled.

If your image is local-only or the container register has auth that you don't
want to deal with, you can do this:

- make sure you have the image on your local machine (pull or build it)
- load the image into minikube
    ```bash
    minikube image load [image tag]
    # e.g.
    minikube image load 111111111111.dkr.ecr.us-east-1.amazonaws.com/yourimage:123
    ```
- check the image is visible inside minikube
    ```bash
    minikube image ls
    ```
- now you can use this image tag in your pipeline


## Bonus: DinD
We can still run a docker-in-docker (dind) image on the k8s cluster with a
Pipeline job like this:
```groovy
podTemplate(yaml: '''
              apiVersion: v1
              kind: Pod
              spec:
                containers:
                - name: docker
                  image: docker:20-dind
                  securityContext:
                    privileged: true
                  env:
                    - name: DOCKER_TLS_CERTDIR
                      value: ""
''') {
    node(POD_LABEL) {
        container('docker') {
            sh '''echo "console.log('hello')" | docker run -i --rm docker.registry.sh.mml.cloud/node:14.17'''
        }
    }
}
```

# Bonus: Scripted Pipeline syntax
This is the same job as in the main list of steps but using the "Scripted
Pipeline" syntax instead of "Declarative Pipeline" syntax.
```
podTemplate(yaml:'''
              spec:
                containers:
                - name: nodey
                  image: node:14.17
                  command:
                  - sleep
                  args: 
                  - 99d
''') {
  node(POD_LABEL) {
    stage('Main') {
      container('nodey') {
        sh 'node --version'
      }
    }
  }
}
```
...or with the deprecated `containerTemplate` syntax:
```groovy
podTemplate(containers: [
  containerTemplate(
    name: 'nodey',
    command: 'sleep',
    args: '99d',
    image: 'node:14.17'
  )
]) {
  node(POD_LABEL) {
    container('nodey') {
      sh 'node --version'
    }
  }
}
```

## Notes on using GKS
FIXME add instructions

You can check actual memory usage with
```
kubectl top pod -n jenkins
```

Get the YAML for a deployed object with
```
kubectl get po -n jenkins jenkins-0 -o yaml
```
